import { Document, Schema, Model, model } from "mongoose";

interface User extends Document {
  _id: string;
  name: string;
}

declare type TUser = Model<User>;

const UserSchema: Schema = new Schema({
  _id: String,
  name: String,
}, {
  id: false,
});


const User: TUser = model<User>('User', UserSchema);

export default User;
