export interface Filters {
  [key: string]: {
    only?: string[];
    except?: string[];
    function?: Function;
  }
}

const obj: Filters = {
  sayHello: {
    only: ['getUsers'],
  },
};

export default obj;
