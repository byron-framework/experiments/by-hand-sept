import { resolve } from 'path';

import filters from './filters';

Object
  .keys(filters)
  .forEach(async (hookName: string): Promise<void> => {
    const path: string = resolve(__dirname, hookName);
    filters[hookName].function = (await import(path)).default;
  });

export const getPreHooks: (commandName: string) => any = (commandName: string): any => {
  const commandHooks: Function[] = Object
    .keys(filters)
    .filter((hookName: string): boolean => {
      const { except, only }: any = filters[hookName];

      if (only !== undefined) {
        return only.includes(commandName);
      }
      
      if (except !== undefined) {
        return !except.includes(commandName);
      }

      return true;
    })
    .map((hookName: string): any => filters[hookName].function );

  return async (parent: any, args: any, context: any, info: any) => {
    commandHooks
      .forEach(async (hook: Function): Promise<void> => {
        await hook(parent, args, context, info);
      });
  };
};
