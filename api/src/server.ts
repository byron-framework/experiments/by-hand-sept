import Koa from 'koa';

import Cors from '@koa/cors';

import { ApolloServer } from 'apollo-server-koa';

import { GraphQLSchema } from 'graphql';
import { importSchema } from 'graphql-import';
import { makeExecutableSchema } from 'graphql-tools';

import uuidv4 from 'uuid/v4';

import { ContextFunction } from 'apollo-server-core';
import { v4 } from 'uuid/interfaces';

import db, { DbConnection } from 'src/db/models/index';
import Event, { IEvent } from 'src/Event';
import resolversPromise from 'src/resolvers/index';

interface Hash {
  [key: string]: any;
}

export interface Context {
  ctx: Hash;
  Event: IEvent;
  uuidv4: v4;
  db: DbConnection;
}

type ServerContextFunction = ContextFunction<Hash, Context>;

const port: number = process.env.PORT ? +process.env.PORT : 3000;

const typeDefs: string = importSchema(`src/graphql/schema.graphql`);

resolversPromise.then((resolvers: any) => {
  Object.keys(resolvers)
    .forEach((key: string) => {
      if (!Object.keys(resolvers[key]).length) {
        delete resolvers[key];
      }
    });

  const schema: GraphQLSchema = makeExecutableSchema({ typeDefs, resolvers });

  const context: ServerContextFunction = ({ ctx }: Hash): Context => ({
    ctx,
    Event,
    uuidv4,
    db,
  });

  const server: ApolloServer = new ApolloServer({
    schema,
    context,
  });

  const app: Koa = new Koa();
  app.use(Cors());
  server.applyMiddleware({ app });

  app.listen({ port }, () => {
    console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`);
  });
});
