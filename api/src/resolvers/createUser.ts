import { ApolloError } from 'apollo-server-koa';

import { Context } from 'src/server';

async function createUser(_parent: any, args: any, ctx: Context, _info: any): Promise<any> {
  const { Event, db: { User }, uuidv4 }: Context = ctx;
  const { name }: any = args;

  let user: any = await User.findOne({ name });

  if (user) {
    console.log(user);
    throw new ApolloError('Name already in use', '409');
  }

  user = {
    name,
    _id: uuidv4(),
  };

  Event.emit('new.user', user._id);
  Event.emit(`user.${user._id}`, { name: user.name });

  return user;
}

export default createUser;
