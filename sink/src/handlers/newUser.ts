import { StanContext } from 'src/sink';

async function newUser(ctx: StanContext): Promise<void> {
  const { Event, db: { User } }: StanContext = ctx;

  console.log('Registering "newUser"');

  await Event.listen('new.user', async (msg: any): Promise<void> => {
    const _id: string = JSON.parse(msg.getData());

    console.log(`Just got ${_id}`);

    await User.create({ _id });

    await Event.listen(`user.${_id}`, async (msg: any): Promise<void> => {
      const change: string = JSON.parse(msg.getData());

      console.log(`Just got ${change}`);

      await User.updateOne({ _id }, change);
    });
  });
}

export default newUser;
