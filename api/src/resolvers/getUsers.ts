import { Context } from "src/server";

async function getUsers(_parent: any, _args: any, ctx: Context, _info: any): Promise<any> {
  const { db: { User } }: Context = ctx;

  return await User.find();
}

export default getUsers;
