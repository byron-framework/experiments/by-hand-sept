import fs from 'fs';

import { Subscription } from 'node-nats-streaming';

import db from 'src/db/models';
import Event, { stan, IEvent } from 'src/Event';

export interface StanContext {
  subs: {
    [key: string]: Subscription;
  };
  db: {
    [key: string]: any;
  };
  Event: IEvent;
}

const stanContext: StanContext = {
  subs: {},
  db,
  Event,
}

const handlers: any[] = [];

fs.readdirSync('./src/handlers')
  .forEach(async (fileName: string): Promise<void> => {
    const name: string = fileName.slice(0, fileName.length - 3);
    const handler: any = await import(`${__dirname}/handlers/${name}`);
    handlers.push(handler.default);
  });

stan.on('connect', (): void => {
  console.log('Sink running!');

  handlers.forEach((handler: any): void => handler(stanContext));
});
