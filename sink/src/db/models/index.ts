import mongoose from 'mongoose';

import fs from 'fs';

fs.readdirSync('./src/db/models')
  .filter((fileName: string) => {
    return !fileName.match('index.ts');
  })
  .forEach(async (fileName: string) => {
    const name: string = fileName.slice(0, fileName.length - 3);
    const model: any = await import(`${__dirname}/${name}`);
    dbConnection[model.default.modelName] = model.default;
  });

const dbConnection: DbConnection = {};

export interface DbConnection {
  [key: string]: any;
}

const cacheUrl: string = process.env.CACHE_URL
  ? process.env.CACHE_URL
  : 'mongodb://cache:27017/test';

mongoose.connect(cacheUrl);

export default dbConnection;
